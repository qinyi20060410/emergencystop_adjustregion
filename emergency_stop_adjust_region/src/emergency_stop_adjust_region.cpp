#include <ros/ros.h>
#include <ros/package.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/time_synchronizer.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/ros/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/geometry.h>
#include <pcl/common/eigen.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/impl/passthrough.hpp>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl_msgs/PointIndices.h>
#include <tf/tf.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Point.h>
#include <cmath>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <visualization_msgs/Marker.h>
#include <std_msgs/String.h>

#include "emergency_stop_adjust_region/emergency_stop_adjust_region.h"

using namespace std;
using namespace sensor_msgs;
using namespace message_filters;

namespace emergency_stop_adjust_region{

    EmergencyStopAD::EmergencyStopAD(ros::NodeHandle private_nh) {
      private_nh.getParam("cloud_topic_name", cloud_topic_name_);
      private_nh.getParam("chasis_topic_name", chassis_topic_name_);
      private_nh.getParam("visual_center_topic_name", visual_center_topic_name_);
      private_nh.getParam("visual_velo_topic_name", visual_velo_topic_name_);
      private_nh.getParam("visual_end_topic_name", visual_end_topic_name_);
      private_nh.getParam("visual_mid_topic_name", visual_mid_topic_name_);
      private_nh.getParam("truck_length", truck_length);
      private_nh.getParam("truck_width", truck_width);
      private_nh.getParam("truck_fd", truck_fd);
      private_nh.getParam("truck_ld", truck_ld);
      private_nh.getParam("truck_L", truck_L);
      private_nh.getParam("truck_n", truck_n);
      private_nh.getParam("truck_m", truck_m);
      private_nh.getParam("distance_alpha", distance_alpha);
      private_nh.getParam("debug", debug);
      private_nh.getParam("visualize", visualize);


      private_nh.getParam("max_filter_x", max_filter_x);
      private_nh.getParam("min_filter_x", min_filter_x);
      private_nh.getParam("max_filter_y", max_filter_y);
      private_nh.getParam("min_filter_y", min_filter_y);
      private_nh.getParam("alpha_y", alpha_y);
      private_nh.getParam("max_filter_z", max_filter_z);
      private_nh.getParam("min_filter_z", min_filter_z);

      left_wheel_position = cv::Point2d(3.576, 1.24);
      right_wheel_position = cv::Point2d(3.576, -1.24);
      left_start_position = cv::Point2d(min_filter_x, max_filter_y);
      right_start_position = cv::Point2d(min_filter_x, min_filter_y);
      left_end_position = cv::Point2d(max_filter_x, max_filter_y);
      right_end_position = cv::Point2d(max_filter_x, min_filter_y);

      pi = 3.141592653589793238463;

      // subscriber
      chassis_msgs_sub_ = private_nh.subscribe(chassis_topic_name_, 1, &EmergencyStopAD::chassis_msgs_callback, this);
      pointcloud_sub_ = private_nh.subscribe(cloud_topic_name_, 1, &EmergencyStopAD::cloud_callback, this);

      // publisher
      visualize_center_pub_ = private_nh.advertise<visualization_msgs::Marker>(visual_center_topic_name_, 10);
      visualize_velo_pub_ = private_nh.advertise<visualization_msgs::Marker>(visual_velo_topic_name_, 10);
      visualize_end_pub_ = private_nh.advertise<visualization_msgs::Marker>(visual_end_topic_name_, 10);
      visualize_mid_pub_ = private_nh.advertise<visualization_msgs::Marker>(visual_mid_topic_name_, 10);
      filtered_cloud_pub_ = private_nh.advertise<sensor_msgs::PointCloud2>("/all_filtered_points", 10);
    }

    EmergencyStopAD::~EmergencyStopAD(){}

    void EmergencyStopAD::test_callback_cloud_chassis(const sensor_msgs::PointCloud2::ConstPtr& test_cloud_msg, const boost::shared_ptr<apollo::canbus::Chassis>& test_chassis_msg){

    }

    void EmergencyStopAD::cloud_callback(const sensor_msgs::PointCloud2::ConstPtr& cloud_msg) {
      if(debug) ROS_INFO_STREAM("Lidar Info message -> start."  );

      pcl::PointCloud<pcl::PointXYZ>::Ptr receive_cloud(new pcl::PointCloud<pcl::PointXYZ>);
      fromROSMsg(*cloud_msg, *receive_cloud);
      if(debug) ROS_INFO_STREAM("Receive Left PointCloud.");

      pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);
      Filter_PointCloud(receive_cloud, filtered_cloud);

      sensor_msgs::PointCloud2 out_msg;
      pcl::toROSMsg(*filtered_cloud, out_msg);
      out_msg.header.frame_id = "chassis";
      filtered_cloud_pub_.publish(out_msg);

      if(debug) ROS_INFO_STREAM("Lidar Info message -> end.");
    }

    void EmergencyStopAD::Filter_PointCloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr& in_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& out_cloud){
      pcl::PointIndices indices;
      indices.indices.clear();
      for(size_t i=0; i< in_cloud->points.size(); i++) {
        auto &p= in_cloud->points[i];
        if(steer_percentage>0)
        {
          if(p.x <= (left_end_position.x + wheel_spd_fl*distance_alpha) &&
          p.x>=left_start_position.x &&
          (pow(p.x-global_center_location.x, 2)+pow(p.y-global_center_location.y, 2))<=pow(global_r[0]+alpha_y, 2) &&
          (pow(p.x-global_center_location.x, 2)+pow(p.y-global_center_location.y, 2))>=pow(global_r[1]-alpha_y, 2)  &&
          p.y>=global_center_location.y && p.z>=min_filter_z && p.z<=max_filter_z)
          {
            indices.indices.push_back(i);
          }
        }
        else
        {
          if(p.x <= (left_end_position.x + wheel_spd_fl*distance_alpha) &&
          p.x>=left_start_position.x &&
          (pow(p.x-global_center_location.x, 2)+pow(p.y-global_center_location.y, 2))>=pow(global_r[0]-alpha_y, 2) &&
          (pow(p.x-global_center_location.x, 2)+pow(p.y-global_center_location.y, 2))<=pow(global_r[1]+alpha_y, 2) &&
          p.y<=global_center_location.y && p.z>=min_filter_z && p.z<=max_filter_z)
          {
            indices.indices.push_back(i);
          }
        }
      }

      pcl::copyPointCloud(*in_cloud, indices.indices, *out_cloud);
      if(out_cloud->points.size() > 0){
        ROS_WARN("size: %d(first position(%f, %f, %f):last position(%f, %f, %f))", int(out_cloud->points.size()),
        float(in_cloud->points[indices.indices.front()].x),
        float(in_cloud->points[indices.indices.front()].y),
        float(in_cloud->points[indices.indices.front()].z),
        float(in_cloud->points[indices.indices.back()].x),
        float(in_cloud->points[indices.indices.back()].y),
        float(in_cloud->points[indices.indices.back()].z));
      }
    }

    void EmergencyStopAD::chassis_msgs_callback(const apollo::canbus::Chassis& chassis_msg){
      if(debug) ROS_INFO_STREAM("Chassis Info message -> start.");

      // time
      // ros::Time time_now(chassis_msg.header().timestamp_sec());

      // wheel_speed
      wheel_spd_rr = chassis_msg.wheel_speed().wheel_spd_rr();
      wheel_spd_rl = chassis_msg.wheel_speed().wheel_spd_rl();
      wheel_spd_fr = chassis_msg.wheel_speed().wheel_spd_fr();
      wheel_spd_fl = chassis_msg.wheel_speed().wheel_spd_fl();

      // steer percentage
      steer_percentage = chassis_msg.steering_percentage();

      // echo
      if(debug) ROS_INFO("wheel_spd_rr: %f", float(wheel_spd_rr));
      if(debug) ROS_INFO("wheel_spd_rl: %f", float(wheel_spd_rl));
      if(debug) ROS_INFO("wheel_spd_fr: %f", float(wheel_spd_fr));
      if(debug) ROS_INFO("wheel_spd_fl: %f", float(wheel_spd_fl));
      if(debug) ROS_INFO("steer_percentage: %f", float(steer_percentage));
      if(debug) ROS_INFO_STREAM("Chassis Info message -> end.");

      // calculate r
      if(debug) ROS_INFO_STREAM("Calculate r -> start.");
      std::vector<double> r = calculate_r(steer_percentage);
      global_r.assign(r.begin(), r.end());
      if(debug) ROS_INFO("left r: %f, ", float(r[0]));
      if(debug) ROS_INFO("right r: %f, ", float(r[1]));
      if(debug) ROS_INFO_STREAM("Calculate r -> end.");

      // calculate center location
      if(debug) ROS_INFO_STREAM("Calculate center location -> start.");
      cv::Point2d center_location = calculate_center_location(steer_percentage, r, left_wheel_position, right_wheel_position);
      global_center_location = center_location;
      if(debug) ROS_INFO("center location: %f, %f", float(center_location.x), float(center_location.y));
      if(debug) ROS_INFO_STREAM("Calculate center location -> end.");

      if(debug) ROS_INFO_STREAM("Calculate end location -> start.");
      std::vector<cv::Point2d> end_point = calculate_end_point(steer_percentage, center_location, r);
      if(debug) ROS_INFO("end location1: %f, %f", float(end_point[0].x), float(end_point[0].y));
      if(debug) ROS_INFO("end location2: %f, %f", float(end_point[1].x), float(end_point[1].y));
      if(debug) ROS_INFO_STREAM("Calculate end location -> end.");

      std::vector<cv::Point2d> mid_points = calculate_mid_point(steer_percentage, center_location, r);

      if(visualize){
        visualize_velo_point(cv::Point2d(5.054, 1.24), cv::Point2d(5.054, -1.24));
        visualize_center_point(center_location);
        visualize_end_point(end_point);
        visualize_mid_point(mid_points);
      }
    }

    double EmergencyStopAD::calculate_angle(double angle_percent){
      double angle = 0;
      // angle
      if(angle_percent>0){
        angle = 90-abs(angle_percent/100*800/23.27);
      }
      else{
        angle = 90+abs(angle_percent/100*800/23.27);
      }
      return angle;
    }

    std::vector<double> EmergencyStopAD::calculate_r(double angle_percent){
      std::vector<double> result;
      double angle = abs(angle_percent/100*800/23.27);
      if(debug) ROS_INFO("steer angle: %f, ", float(angle));

      double R_Left, R_Right;
      if(angle_percent>0){
        // R_Left = truck_L/sin(angle*pi/180.0);
        // R_Left = sqrt( pow(truck_L+truck_fd, 2) + pow(truck_L/tan(angle*pi/180.0)+(truck_width-truck_n)/2, 2));
        // if(debug) ROS_INFO("R_Left: %f, ", float(R_Left));

        // R_Right = truck_L/sin(angle*pi/180.0);
        // R_Right = sqrt( pow(truck_L, 2) + pow(truck_L/tan(angle*pi/180.0)-(truck_width-truck_n)/2, 2));
        // if(debug) ROS_INFO("R_Right: %f, ", float(R_Right));

        R_Left = sqrt( pow(truck_L+truck_fd, 2) + pow(truck_L/tan(angle*pi/180.0)+(truck_width-truck_n)/2, 2));
        if(std::isinf(R_Left)){
          R_Left = 999999;
        }
        R_Right = R_Left - truck_width;
        if(debug) ROS_INFO("R_Left: %f, ", float(R_Left));
        if(debug) ROS_INFO("R_Right: %f, ", float(R_Right));
      }

      else if(angle_percent<=0){
        // R_Right = truck_L/sin(angle*pi/180.0);
        // R_Right = sqrt( pow(truck_L+truck_fd, 2) + pow(truck_L/tan(angle*pi/180.0)+(truck_width-truck_n)/2, 2));
        // if(debug) ROS_INFO("R_Right: %f, ", float(R_Right));

        // R_Left = truck_L/sin(angle*pi/180.0);
        // R_Left = sqrt( pow(truck_L, 2) + pow(truck_L/tan(angle*pi/180.0)-(truck_width-truck_n)/2, 2));
        // if(debug) ROS_INFO("R_Left: %f, ", float(R_Left));

        R_Right = sqrt( pow(truck_L+truck_fd, 2) + pow(truck_L/tan(angle*pi/180.0)+(truck_width-truck_n)/2, 2));
        if(std::isinf(R_Right)){
          R_Right = 999999;
        }
        R_Left = R_Right - truck_width;
        if(debug) ROS_INFO("R_Left: %f, ", float(R_Left));
        if(debug) ROS_INFO("R_Right: %f, ", float(R_Right));
      }

      result.push_back(R_Left);
      result.push_back(R_Right);
      return result;
    }

    cv::Point2d EmergencyStopAD::calculate_center_location(double angle_percent, std::vector<double> r, cv::Point2d left_point, cv::Point2d right_point){
      double angle = 0;
      double location_angle = 0;
      double x, y;
      // angle
      if(angle_percent>0){
        angle = 90-abs(angle_percent/100*800/23.27);
        location_angle = angle+270;

        cv::Point2d point = left_point;

        if(debug) ROS_INFO("r: %f", float(r[0]));

        if(debug) ROS_INFO("r*sin: %f", float(r[0]*sin(location_angle*pi/180.0)));
        if(debug) ROS_INFO("r*cos: %f", float(-r[0]*cos(location_angle*pi/180.0)));

        x = point.x + r[0]*sin(location_angle*pi/180.0);
        y = point.y - r[0]*cos(location_angle*pi/180.0);
      }
      else{
        angle = 90+abs(angle_percent/100*800/23.27);
        location_angle = angle+90;

        cv::Point2d point = right_point;

        if(debug) ROS_INFO("r: %f", float(r[1]));

        if(debug) ROS_INFO("r*sin: %f", float(r[1]*sin(location_angle*pi/180.0)));
        if(debug) ROS_INFO("r*cos: %f", float(-r[1]*cos(location_angle*pi/180.0)));

        x = point.x + r[1]*sin(location_angle*pi/180.0);
        y = point.y - r[1]*cos(location_angle*pi/180.0);
      }

      if(debug) ROS_INFO("center location: %f, %f", float(x), float(y));
      return cv::Point2d(x,y);
    }

    std::vector<cv::Point2d> EmergencyStopAD::calculate_end_point(double angle_percent, cv::Point2d center_location, std::vector<double> r){
      double x;
      double y_left, y_right;

      // calculate Intersection
      if(angle_percent>0){
        x = left_end_position.x + wheel_spd_fl*distance_alpha;// - angle_percent/10;
        y_left = sqrt(pow(r[0]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        y_right = sqrt(pow(r[1]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
      }
      else{
        x = left_end_position.x + wheel_spd_fl*distance_alpha;// + angle_percent/10;
        y_left = -sqrt(pow(r[0]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        y_right = -sqrt(pow(r[1]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
      }

      std::vector<cv::Point2d> result;
      result.push_back(cv::Point2d(x, y_left));
      result.push_back(cv::Point2d(x, y_right));
      return result;
    }


    std::vector<cv::Point2d> EmergencyStopAD::calculate_mid_point(double angle_percent, cv::Point2d center_location, std::vector<double> r){
      double x;
      double y_left, y_right;
      std::vector<cv::Point2d> result;

      // calculate Intersection
      if(angle_percent>0){
        x = left_start_position.x;
        y_left = sqrt(pow(r[0]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_start_position.x+1;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_left = sqrt(pow(r[0]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_start_position.x+2;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_left = sqrt(pow(r[0]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_start_position.x+3;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_left = sqrt(pow(r[0]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_start_position.x+4;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_left = sqrt(pow(r[0]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_end_position.x + wheel_spd_fl*distance_alpha;// - angle_percent/10;
        y_left = sqrt(pow(r[0]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_end_position.x + wheel_spd_fl*distance_alpha;// - angle_percent/10;
        y_right = sqrt(pow(r[1]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x + 4;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_right = sqrt(pow(r[1]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x + 3;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_right = sqrt(pow(r[1]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x + 2;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_right = sqrt(pow(r[1]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x + 1;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_right = sqrt(pow(r[1]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_right = sqrt(pow(r[1]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_left = sqrt(pow(r[0]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));
      }
      else{
        x = left_start_position.x;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_left = -sqrt(pow(r[0]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_start_position.x + 1;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_left = -sqrt(pow(r[0]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_start_position.x + 2;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_left = -sqrt(pow(r[0]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_start_position.x + 3;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_left = -sqrt(pow(r[0]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_start_position.x + 4;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_left = -sqrt(pow(r[0]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_end_position.x + wheel_spd_fl*distance_alpha;// - angle_percent/10;
        y_left = -sqrt(pow(r[0]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));

        x = left_end_position.x + wheel_spd_fl*distance_alpha;// - angle_percent/10;
        y_right = -sqrt(pow(r[1]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x + 4;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_right = -sqrt(pow(r[1]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x + 3;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_right = -sqrt(pow(r[1]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x + 2;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_right = -sqrt(pow(r[1]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x + 1;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_right = -sqrt(pow(r[1]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_right = -sqrt(pow(r[1]+alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_right));

        x = left_start_position.x;
        if(x>=max_filter_x){
          x=max_filter_x;
        }
        y_left = -sqrt(pow(r[0]-alpha_y, 2) - pow(x-center_location.x, 2)) + center_location.y;
        result.push_back(cv::Point2d(x, y_left));
      }
      return result;
    }

    void EmergencyStopAD::visualize_center_point(cv::Point2d center_location){
      visualization_msgs::Marker points;
      points.header.frame_id = "/chassis";
      // points.header.stamp = ros::Time::now();
      points.ns = "center_points";
      points.action = visualization_msgs::Marker::ADD;
      points.pose.orientation.w = 1;
      points.id = 0;
      points.type = visualization_msgs::Marker::POINTS;

      points.scale.x = 0.5;
      points.scale.y = 0.5;

      points.color.g = 1.0;
      points.color.a = 1.0;

      geometry_msgs::Point p;
      p.x = center_location.x;
      p.y = center_location.y;
      p.z = 0;
      if(isfinite(p.x) && isfinite(p.y) && isfinite(p.z)){
        points.points.push_back(p);
      }

      visualize_center_pub_.publish(points);
    }

    void EmergencyStopAD::visualize_mid_point(std::vector<cv::Point2d> center_location){
      visualization_msgs::Marker points, line_strip, line_list, line_strip_;
      points.header.frame_id = line_strip.header.frame_id = line_strip_.header.frame_id = line_list.header.frame_id = "/chassis";
      points.header.stamp = line_strip.header.stamp = line_strip_.header.stamp = line_list.header.stamp= ros::Time::now();
      points.ns = line_strip.ns = line_strip_.ns= line_list.ns = "mid_points_and_lines";
      points.action = line_strip.action = line_strip_.action = line_list.action = visualization_msgs::Marker::ADD;
      points.pose.orientation.w = line_strip.pose.orientation.w = line_strip_.pose.orientation.w= line_list.pose.orientation.w = 1;
      points.id = 0;
      line_strip.id = 1;
      line_strip_.id = 3;
      line_list.id = 2;
      points.type = visualization_msgs::Marker::POINTS;
      line_strip.type = visualization_msgs::Marker::LINE_STRIP;
      line_strip_.type = visualization_msgs::Marker::LINE_STRIP;
      line_list.type = visualization_msgs::Marker::LINE_LIST;

      points.scale.x = 0.25;
      points.scale.y = 0.25;

      line_strip.scale.x = 0.25;
      line_strip_.scale.x = 0.25;
      line_list.scale.x = 0.25;

      points.color.g = 1.0;
      points.color.a = 1.0;

      line_strip.color.b = 1.0;
      line_strip.color.a = 1.0;

      line_strip_.color.b = 1.0;
      line_strip_.color.a = 1.0;

      line_list.color.r = 1.0;
      line_list.color.a = 1.0;

      geometry_msgs::Point p;
      geometry_msgs::Point q;
      for(std::vector<cv::Point2d>::iterator it=center_location.begin();it<center_location.end();it++){
        p.x = q.x = (*it).x;
        p.y = q.y = (*it).y;
        p.z = min_filter_z;
        if(isfinite(p.x) && isfinite(p.y) && isfinite(p.z)){
          points.points.push_back(p);
          line_strip.points.push_back(p);
          line_list.points.push_back(p);
          p.z = q.z = max_filter_z;
          line_list.points.push_back(p);
          line_strip_.points.push_back(q);
        }
      }
      visualize_mid_pub_.publish(points);
      visualize_mid_pub_.publish(line_strip);
      visualize_mid_pub_.publish(line_strip_);
      visualize_mid_pub_.publish(line_list);
    }

    void EmergencyStopAD::visualize_end_point(std::vector<cv::Point2d> center_location){
      visualization_msgs::Marker points;
      points.header.frame_id = "/chassis";
      // points.header.stamp = ros::Time::now();
      points.ns = "end_points";
      points.action = visualization_msgs::Marker::ADD;
      points.pose.orientation.w = 1;
      points.id = 0;
      points.type = visualization_msgs::Marker::POINTS;

      points.scale.x = 0.25;
      points.scale.y = 0.25;

      points.color.r = 1.0;
      points.color.a = 1.0;

      geometry_msgs::Point p;
      for(std::vector<cv::Point2d>::iterator it=center_location.begin();it<center_location.end();it++){
        p.x = (*it).x;
        p.y = (*it).y;
        p.z = min_filter_z;
        if(isfinite(p.x) && isfinite(p.y) && isfinite(p.z)){
          points.points.push_back(p);
        }
      }
      visualize_end_pub_.publish(points);
    }

    void EmergencyStopAD::visualize_velo_point(cv::Point2d center_location1, cv::Point2d center_location2){
      visualization_msgs::Marker points;
      points.header.frame_id = "/chassis";
      // points.header.stamp = ros::Time::now();
      points.ns = "velo_points";
      points.action = visualization_msgs::Marker::ADD;
      points.pose.orientation.w = 1;
      points.id = 0;
      points.type = visualization_msgs::Marker::POINTS;

      points.scale.x = 2;
      points.scale.y = 2;

      points.color.b = 1.0;
      points.color.a = 1.0;

      geometry_msgs::Point p;
      p.x = center_location1.x;
      p.y = center_location1.y;
      p.z = 0;
      if(isfinite(p.x) && isfinite(p.y) && isfinite(p.z)){
        points.points.push_back(p);
      }

      p.x = center_location2.x;
      p.y = center_location2.y;
      p.z = 0;
      if(isfinite(p.x) && isfinite(p.y) && isfinite(p.z)){
        points.points.push_back(p);
      }

      visualize_velo_pub_.publish(points);
    }
}


int main(int argc, char **argv) {

  ros::init(argc, argv, "emergency_stop_adjust_region_node");

  ros::NodeHandle hn("~");
  // ros::NodeHandle n;

  emergency_stop_adjust_region::EmergencyStopAD emc_stop(hn);
  ros::spin();

  return 0;
}