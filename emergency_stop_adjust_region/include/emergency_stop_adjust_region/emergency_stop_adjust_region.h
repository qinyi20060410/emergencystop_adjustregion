#ifndef EMERGENCY_STOP_ADJUST_REGION_H_
#define EMERGENCY_STOP_ADJUST_REGION_H_

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include "modules/canbus/proto/chassis.pb.h"
#include <math.h>
#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <vector>
#include <cmath>
#include <unistd.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/eigen.h>
#include <pcl/common/transforms.h>
#include <tf/tf.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <ros/ros.h>
#include <geometry_msgs/Point.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <cmath>
#include <std_msgs/String.h>

using namespace std;

namespace Velodyne{
    static const int RINGS_COUNT = 16;

    struct Point{
        PCL_ADD_POINT4D;
        float intensity;
        uint16_t ring;
        float range;
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    }EIGEN_ALIGN16;

    void addRange(pcl::PointCloud<Velodyne::Point> & pc){
        for (pcl::PointCloud<Point>::iterator pt = pc.points.begin(); pt < pc.points.end(); pt++){
            pt->range = sqrt(pt->x * pt->x + pt->y * pt->y + pt->z * pt->z);
        }
    }

    vector<vector<Point*>> getRings(pcl::PointCloud<Velodyne::Point> & pc){
        vector<vector<Point*> > rings(RINGS_COUNT);
        for (pcl::PointCloud<Point>::iterator pt = pc.points.begin(); pt < pc.points.end(); pt++){
            rings[pt->ring].push_back(&(*pt));
        }
        return rings;
    }

    void normalizeIntensity(pcl::PointCloud<Point> & pc, float minv, float maxv){
        float min_found = INFINITY;
        float max_found = -INFINITY;

        for (pcl::PointCloud<Point>::iterator pt = pc.points.begin(); pt < pc.points.end(); pt++){
            max_found = max(max_found, pt->intensity);
            min_found = min(min_found, pt->intensity);
        }

        for (pcl::PointCloud<Point>::iterator pt = pc.points.begin(); pt < pc.points.end(); pt++){
            pt->intensity = (pt->intensity - min_found) / (max_found - min_found) * (maxv - minv) + minv;
        }
    }
}

POINT_CLOUD_REGISTER_POINT_STRUCT(
  Velodyne::Point, (float, x, x) (float, y, y) (float, z, z)
  (float, intensity, intensity) (uint16_t, ring, ring) (float, range, range));

namespace emergency_stop_adjust_region{
    class EmergencyStopAD{
        public:
            EmergencyStopAD(ros::NodeHandle private_nh);
            ~EmergencyStopAD();
        private:
             // topic
            std::string chassis_topic_name_, cloud_topic_name_;
            std::string visual_center_topic_name_, visual_velo_topic_name_, visual_end_topic_name_, visual_mid_topic_name_;

            // test
            std::string test_cloud_topic_name_, test_chassis_topic_name_;
            typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::PointCloud2, boost::shared_ptr<apollo::canbus::Chassis> > test_syncPolicy_cloud_chassis;
            message_filters::Subscriber<sensor_msgs::PointCloud2>* test_cloud_sub_; // topic1
            message_filters::Subscriber<boost::shared_ptr<apollo::canbus::Chassis>>* test_chasssis_msgs_sub_; //topic2
            message_filters::Synchronizer<test_syncPolicy_cloud_chassis>* test_sync_cloud_chassis_; //sync
            void test_callback_cloud_chassis(const sensor_msgs::PointCloud2::ConstPtr& test_cloud_msg, const boost::shared_ptr<apollo::canbus::Chassis>& test_chassis_msg);

            // subscriber
            ros::Subscriber left_pointcloud_sub_, right_pointcloud_sub_, chassis_msgs_sub_, pointcloud_sub_;

            // publisher
             ros::Publisher visualize_center_pub_, visualize_velo_pub_, visualize_end_pub_, visualize_mid_pub_, filtered_cloud_pub_;

            // calculate angle
            double calculate_angle(double angle_percent);

            // calcuate r
            std::vector<double> calculate_r(double angle_percent);

            // calculate location
            cv::Point2d calculate_center_location(double angle_percent, std::vector<double> r, cv::Point2d left_point, cv::Point2d right_point);
            std::vector<cv::Point2d> calculate_end_point(double angle_percent, cv::Point2d center_location, std::vector<double> r);
            std::vector<cv::Point2d> calculate_mid_point(double angle_percent, cv::Point2d center_location, std::vector<double> r);

            void Filter_PointCloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr& in_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& out_cloud);

            // visualize
            void visualize_center_point(cv::Point2d center_location);
            void visualize_end_point(std::vector<cv::Point2d> center_location);
            void visualize_mid_point(std::vector<cv::Point2d> center_location);
            void visualize_velo_point(cv::Point2d center_location1, cv::Point2d center_location2);

            // callback function
            void cloud_callback(const sensor_msgs::PointCloud2::ConstPtr& cloud_msg);
            void chassis_msgs_callback(const apollo::canbus::Chassis& in_chassis);

            // param
            double wheel_spd_rr, wheel_spd_rl, wheel_spd_fr, wheel_spd_fl;
            double steer_percentage;
            double truck_length, truck_width, truck_fd, truck_ld, truck_L, truck_n, truck_m;
            double distance_alpha;
            bool debug;
            bool visualize;

            cv::Point2d left_wheel_position, right_wheel_position;
            cv::Point2d left_start_position, right_start_position;
            cv::Point2d left_end_position, right_end_position;

            // transform
            tf::Transformer frame_transformer;
            tf::TransformListener listener;
            tf::StampedTransform left_point_transform, right_point_transform;

            double pi;
            double alpha;
            double max_filter_x, min_filter_x, max_filter_y, min_filter_y, max_filter_z, min_filter_z, alpha_y;


            cv::Point2d global_center_location;
            std::vector<double> global_r;
    }; //calss EmergencyStopAD
}
#endif